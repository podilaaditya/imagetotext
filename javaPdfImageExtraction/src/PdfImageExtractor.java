

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.lang.String;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

import javax.imageio.ImageIO;


@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
public class PdfImageExtractor {

    public static void main(String[] args) {
        try {
            String sourceDir = "/Users/apodila/Downloads/Demo";// Paste pdf files in PDFCopy folder to read
            String destinationDir = "/Volumes/Data/Work/GitHub/Projects/Project-pdfextraction/codetrials/Demo/";

            File[]  lFileList = getFileListToParse(sourceDir);
            for(File lFile : lFileList) {

                if (lFile.exists()) {
                    PDDocument document = PDDocument.load(lFile);

                    StoreEachPageAsImages(document, lFile.getName(),  destinationDir);
                    //System.out.println("lImageList Count = " + lImageList.size());
                    //saveImagefiles(lImageList);
                    document.close();
                }
                else {
                    System.err.println("File not exists");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    private static void StoreEachPageAsImages(PDDocument document, String aFileName,String aDestinationPath) throws IOException {

        //PDDocument document = PDDocument.load(new File(pdfFilename));
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        for (int page = 0; page < document.getNumberOfPages(); ++page)
        {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

            // suffix in filename will be used as the file format
            ImageIOUtil.writeImage(bim, aDestinationPath + aFileName + "-" + (page+1) + ".png", 300);
        }

    }

    private static void saveImagefiles(List<BufferedImage> images) {

        try {
            int Cntr = 0;
            for(BufferedImage Element : images) {
                //String fileName = generateFileName();
                String lFileName =
                        "/Volumes/Data/Work/GitHub/Projects/Project-pdfextraction/codetrials/files/"+ new Date().getTime() + ".png";
                System.out.println("lFileName = " + lFileName);

                File fObj = new File(lFileName);
                ImageIO.write(Element, "PNG",fObj);

                Cntr++;
            }
        }
        catch (Exception excepObj) {
            excepObj.printStackTrace();
        }

    }

    public  static  List<BufferedImage>   getImagesFromPDF(PDDocument document) throws IOException {
        List<BufferedImage> images = new ArrayList<>();
        for (PDPage page : document.getPages()) {
            images.addAll(getImagesFromResources(page.getResources()));
        }

        return images;
    }

    private static  List<BufferedImage> getImagesFromResources(PDResources resources) throws IOException {
        List<BufferedImage> images = new ArrayList<>();

        for (COSName xObjectName : resources.getXObjectNames()) {
            PDXObject xObject = resources.getXObject(xObjectName);

            if (xObject instanceof PDFormXObject) {
                images.addAll(getImagesFromResources(((PDFormXObject) xObject).getResources()));
            } else if (xObject instanceof PDImageXObject) {
                images.add(((PDImageXObject) xObject).getImage());
            }
        }

        return images;
    }


    private static  File[] getFileListToParse(String aPathOfFiles) {
        File folder = new File(aPathOfFiles);
        File[] files = null;
        // check if the specified pathname is directory first
        if (folder.isDirectory()) {
            //list all files on directory
            files = folder.listFiles(new FilenameFilter() {

                //apply a filter
                @Override
                public boolean accept(File dir, String name) {
                    boolean result;
                    if (name.endsWith(".pdf")) {
                        result = true;
                    } else {
                        result = false;
                    }
                    return result;
                }
            });
            //print all files on the directory
            for (File f : files) {
                try {
                    System.out.println(f.getCanonicalPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



        }
        return files;
    }


    //private static

    private String mLabel;
    //private s
}


/*
* 1. get all the files in the folder make list of it
* 2. gget through
* 3.




*
*
* */